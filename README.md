
# Membership Inference Attacks
 As part of the Advanced Security and Privacy final project we provide an implementation of the membership inference attack. We use the python package created by gihub user BielStela.

 **Gihub repository**: https://github.com/BielStela/membership_inference

 We implement the membership inference attack on breast cancer data available in UCI machine learning repository. (https://archive.ics.uci.edu/ml/datasets/breast+cancer)

 The implementation has been provided in a jupyter notebook named demo.ipynb and can be found in the root directory of the project.